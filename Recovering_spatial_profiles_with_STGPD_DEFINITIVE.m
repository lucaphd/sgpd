% Recovering spatial profiles
close all

%% Spatial covariance function (Interpolator)
dx = 0.1;
spatial_range = -2:dx:2;
extended_spatial_range = -15:dx:15;

l = 0.15;
order = 3;
spatial_frequency_lim = 1/(2*dx);
spatial_frequency_step = 1/(max(extended_spatial_range) - min(extended_spatial_range));
spatial_frequency_range = -spatial_frequency_lim:spatial_frequency_step:spatial_frequency_lim;
frequency_covariance = 1./sqrt(1 + (spatial_frequency_range/l).^(2*order));
space_covariance = real(fftshift(fft(ifftshift(frequency_covariance))));
space_covariance = space_covariance/max(space_covariance);

figure, plot(extended_spatial_range, space_covariance)

%% Spatial grid
x_range = -2:dx:2;
y_range = -2:dx:2;

first_coo = repmat(x_range, [1,length(y_range)]);
second_coo = [];
for index = 1:length(x_range)
    second_coo = [second_coo, y_range(index)*ones([1,length(x_range)])];
end

distance_matrix = zeros(length(x_range)*length(y_range));
for index_1 = 1:length(first_coo)
    for index_2 = 1:length(second_coo)
        distance_matrix(index_1, index_2) = sqrt(sum(([first_coo(index_1), second_coo(index_1)] - [first_coo(index_2), second_coo(index_2)]).^2));
    end
end

figure,
imagesc(distance_matrix)

%% Spatial covariance matrix
covariance_matrix = interp1(extended_spatial_range,space_covariance,distance_matrix);

figure,
imagesc(covariance_matrix)

%% Source spatial profile
[meshgrid_1, meshgrid_2] = meshgrid(x_range, y_range);

loc_1 = [1,1]';
loc_2 = [-1,-1]';
spread = 0.2;
alpha_spatial_profile = exp(-0.5*((meshgrid_1 - loc_1(1)).^2 + (meshgrid_2 - loc_1(2)).^2)/spread^2) + exp(-0.5*((meshgrid_1 - loc_2(1)).^2 + (meshgrid_2 - loc_2(2)).^2)/spread^2);
vec_alpha_profile = reshape(alpha_spatial_profile,[numel(alpha_spatial_profile),1]);

rel_loc_1 = [1,-1]';
rel_loc_2 = [-1,1]';
rel_spread = 0.8;
rel_spatial_profile = exp(-0.5*((meshgrid_1 - rel_loc_1(1)).^2 + (meshgrid_2 - rel_loc_1(2)).^2)/rel_spread^2) + exp(-0.5*((meshgrid_1 - rel_loc_2(1)).^2 + (meshgrid_2 - rel_loc_2(2)).^2)/rel_spread^2);
vec_rel_profile = reshape(rel_spatial_profile,[numel(rel_spatial_profile),1]);

figure,
imagesc(alpha_spatial_profile)

figure,
plot(vec_alpha_profile)
hold on 
plot(vec_rel_profile)

%% Signal generation

% Constants
dt = 0.01;
T = 1;
t = 0:dt:0.8;
t_test_ind = find(t>0.1&t<0.5);
N = 1;
number_trials = 600;
number_time_points = length(t);
[T_x, T_y] = meshgrid(t,t); % For constructing the kernel
exponent = 1.;
extended_spatial_range = -9:dx:9;
length_scale = 1.;
oscillation_length_scale = 2.; % 1. or 2.

spatiotemporal_signal = cell(number_trials,1);
spatiotemporal_alpha_process = cell(number_trials,1);
alpha_process = cell(number_trials,1);
concatenated_data = [];
for trial = 1:number_trials
    % Alpha process
    % covariance function
    amp_1 = 0.5; %variance
    nu_1 = 0.05; % length scale
    K_amp = amp_1.^2*exp(-(T_x-T_y).^2/nu_1);
    % mean function
    m_1 = ones(length(t),1);
    r1 = mvnrnd(m_1,K_amp,N);
    p1 = sqrt(1+r1.^2) - 1;
    omega = ones(length(t),1)*10*2*pi;
    sd_2 = 0.25*2*pi; %0.4
    nu_2 = 0.04; % frequency length scale
    K_freq = sd_2.^2*exp(-(T_x-T_y).^2/nu_2);
    r2 = mvnrnd(omega,K_freq,N);
    p2 = cos(r2.*repmat(t,N,1) + 2*pi*repmat(rand(N,1),[1,length(t)]));
    alpha_process{trial} = (p1.*p2.^exponent)';
    alpha_length_scale = oscillation_length_scale;
    alpha_center = -0.5;
    alpha_spatial_profile = exp(-abs(spatial_range - alpha_center).^2/(2*alpha_length_scale^2));
    spatiotemporal_alpha_process{trial} = (alpha_process{trial}*vec_alpha_profile')';

    % Broadband noise
    % covariance function
    amp_4 = 0.3; %variance
    spatiotemporal_bb_process = amp_4*randn(size(spatiotemporal_alpha_process{trial}));

    % Relaxation noise 
    % covariance function
    amp_5 = 0.4; %variance
    k_5 = 0.1; % length scale
    K_bn = amp_5.^2*exp(-abs(T_x-T_y)/k_5);
    relaxation_noise = mvnrnd(zeros(length(t),1),K_bn,N)';
    rel_length_scale = length_scale;
    rel_center = 1.5;
    rel_spatial_profile = exp(-abs(spatial_range - rel_center).^2/(2*rel_length_scale^2));
    spatiotemporal_rel_process = (relaxation_noise*vec_rel_profile')';

    % Total signal
    spatiotemporal_signal{trial} = spatiotemporal_alpha_process{trial} + spatiotemporal_bb_process + spatiotemporal_rel_process;
    concatenated_data = [concatenated_data, spatiotemporal_signal{trial}];
end

figure, imagesc(spatiotemporal_signal{1})
figure, plot(t, spatiotemporal_signal{1})

%% Signal covariance function
c_1 = 0;
for trial = 1:number_trials
    demeaned_signal = spatiotemporal_signal{trial}; %detrend(spatiotemporal_signal{trial}','constant')';
    c_1 = c_1 + demeaned_signal'*demeaned_signal/number_trials;
end

% stationarization
c_stat_1 = 0;
for j = -length(t):length(t)
    c_stat_1 = c_stat_1 + diag(mean(diag(c_1,j))*ones(length(diag(c_1,j)),1),j);
end

max_c_1 = max(max(c_stat_1));
c_norm_1 = c_stat_1/max_c_1;

figure,
plot(diag(fliplr(c_norm_1)))

%% GP regression model

% Priors parameters

% Kernel configuration
num_kernels = 3; % Number of kernels
kernels = {'exponential', 'damped_harmonic_oscillator', 'squared_exponential'}; % Type of kernels

% Prior configuration
cfg_param = [];
num_param = [ 2, 3, 2];
% Each hyper-parameter can have its own (hyper-)prior distribution
prior_pdf{1} = {'logNorm', 'unif'}; %Exponential
prior_pdf{2} = {'logNorm', 'logNorm', 'beta'};
prior_pdf{3} = {'logNorm','logNorm'};


% Hyper-prior parameters
prior_hyp_parameters{1} = { [ 1, 2], [0, 5]}; %Exponential
prior_hyp_parameters{2} = { [ 1, 2], [0.01, 40], [2*pi*9, 2*pi*13, 1, 1]}; %Alpha 
prior_hyp_parameters{3} = { [ 1, 2], [ 0, 1]}; 

% Variable transformations
var_transform{1} = {'log', 'logit'};
var_transform{2} = {'log','log', 'logit' };
var_transform{3} = {'log','logit'};

transform_param{1} = {[], [0, 5]};
transform_param{2} = {[], [], [2*pi*9, 2*pi*13] };
transform_param{3} = {[], [10^-30,10^-29]};

for j = 1:num_kernels
    % Parameters structure
    cfg = [];
    cfg.numparam = num_param(j); % number of (hyper-)parameters
    cfg.pdf = prior_pdf{j};
    cfg.hyparam = prior_hyp_parameters{j};
    cfg.tran = var_transform{j};
    cfg.tranparam = transform_param{j}; 
    cfg_param = setfield(cfg_param, kernels{j}, cfg);
end

% Kernel structure
cfg_k = [];
cfg_k.kernels = kernels;
cfg_k.num = num_kernels;
cfg_k.num_param = max(num_param);
% cfg_k.wavelets.centers = 0.1:0.05:0.2;
% cfg_k.wavelets.scales = (2*pi*[ 2 ]).^-1;
% cfg_k.wavelets.width = sqrt(0.05);
% cfg_k.wavelets.amp = 0.2;

% Parameters initialization
in_param = [log(1/4), log(1), log(1/4), 0, 0, log(1/4), 0  ]'  + 0.1*randn(sum(num_param),1);
                  
%% Simulated annealing 
% Finding the global mode that will be also used as a starting point for
% the monte carlo sampling
sigma = 0.1;

l = @(x) ct_least_squares4(cfg_k, cfg_param, x, c_norm_1, T_x, T_y);
g = @(x) x + randn(1)*sigma*mnrnd(1,ones(size(x))/length(x)).*trnd(2);

cfg = [];
cfg.Verbosity = 2;
cfg.InitTemp = 10;
cfg.StopTemp = 10^-12;cfg.Generator = g;
cfg.CoolSched = @(T) (.95*T);
mode = anneal(l,in_param',cfg);
lambda_n = 0.1;

%% Covariance function plot
theta = ct_transform(cfg_k, cfg_param, mode);
[K_tot K] = ct_kernel(cfg_k, theta, T_x, T_y, 0);
K_tot = max_c_1*K_tot;
K = max_c_1*K;

tau = (T_x - T_y);
k_plot = [ fliplr(K_tot(2:end,1)'), K_tot(1,1:end)];
c_plot = [ fliplr(c_norm_1(2:end,1)'), c_norm_1(1,1:end)];
t_plot = [ fliplr(tau(2:end,1)'), tau(1,1:end)];

figure, 
plot(t_plot, max_c_1*c_plot, t_plot, k_plot)
legend('Data autocovariance','Parametric fitting')

%% Posterior mean
spatial_noise = 0.1;
total_covariance_matrix = covariance_matrix + spatial_noise*eye(length(x_range)*length(y_range));
spatial_filter = covariance_matrix/total_covariance_matrix;

mean_pos = cell(number_trials,1);
mean_process = cell(number_trials,1);

[K_tot, K] = ct_kernel(cfg_k, ct_transform(cfg_k, cfg_param, mode), T_x, T_y, 0);
kernel = 2;
mean_pos = cell(number_trials,1);
for trial = 1:number_trials
    inv_K = squeeze(K(kernel,:,:))/K_tot;
    mean_pos{trial} = spatial_filter*spatiotemporal_signal{trial}*inv_K';
    [max_val, max_ind] = max(sum(abs(mean_pos{1}')));
    mean_process{trial} = mean_pos{trial}(max_ind,:);
end


figure,
imagesc(mean_pos{1})

%% Recovering spatial profile
amplitude_maps = cell(number_trials,1);
GP_similarities = zeros(number_trials,1);
for trial = 1:number_trials
    vec_map = sum(abs(hilbert(mean_pos{trial}')),1);
    amplitude_maps{trial} = reshape(vec_map, size(meshgrid_1));
    %GP_similarities(trial) = abs(dot(vec_map, vec_alpha_profile)/(norm(vec_map)*norm(vec_alpha_profile)));
    GP_similarities(trial) = abs(corr(vec_map', vec_alpha_profile));
end

figure,
imagesc(amplitude_maps{5})

%% SVD analysis
PCA_spatial_profiles = cell(number_trials,1);
PCA_similarities = zeros(number_trials,1);

for trial = 1:number_trials
    [PCA_maps,D,PCA_comps] = svd(detrend(spatiotemporal_signal{trial}','constant')');
    PCA_comps = PCA_comps';

    number_comps = size(PCA_comps,1);
    c = zeros(number_comps,1);
    for component = 1:number_comps
        c(component) = corr(PCA_comps(component,:)', alpha_process{trial});
    end
    [~, max_ind] = max(abs(c));
    vec_map = PCA_maps(:,max_ind);
    PCA_spatial_profiles{trial} = reshape(vec_map, size(meshgrid_1));
    %PCA_similarities(trial) = abs(dot(vec_map, vec_alpha_profile)/(norm(vec_map)*norm(vec_alpha_profile)));
    PCA_similarities(trial) = abs(corr(vec_map, vec_alpha_profile));
end

figure,
imagesc(PCA_spatial_profiles{3})

%% ICA
ICA_spatial_profiles = cell(number_trials,1);
ICA_similarities = zeros(number_trials,1);

ICA_comps = fastica(detrend(concatenated_data','constant')', 'numOfIC', 10, 'LastEig', 10);

number_comps = size(ICA_comps,1);
c = zeros(number_comps,1);
for component = 1:number_comps
    c(component) = corr(ICA_comps(component,1:length(alpha_process{1}))', alpha_process{1});
end
 
[max_val, max_ind] = max(abs(c));
sign_corr = sign(c(max_ind));
ICA_selected_comp = sign_corr*ICA_comps(max_ind,:);
figure, 
plot(zscore(ICA_selected_comp(1:80)))
hold on
plot(zscore(alpha_process{1}(1:80)))

%%
for trial = 1:number_trials
    trial_comp = ICA_selected_comp((trial - 1)*number_time_points + 1:trial*number_time_points);
    vec_map = spatiotemporal_signal{trial}/trial_comp;
    ICA_spatial_profiles{trial} = reshape(vec_map, size(meshgrid_1));
    %ICA_similarities(trial) = abs(dot(vec_map, vec_alpha_profile)/(norm(vec_map)*norm(vec_alpha_profile)));
    ICA_similarities(trial) = abs(corr(vec_map, vec_alpha_profile));
end

%%

figure,
h = boxplot([GP_similarities, PCA_similarities, ICA_similarities],'labels',{['GP = ', num2str(median(GP_similarities))],['PCA = ', num2str(median(PCA_similarities))] ,['ICA = ', num2str(median(ICA_similarities))]});
set(h(7,:),'Visible','off') 
ylim([0.4,1])
%scatter(GP_similarities, ICA_similarities)
%hold on
%plot(0:0.01:1,0:0.01:1)
%xlim(0.5,1)
%ylim(0.5,1)







