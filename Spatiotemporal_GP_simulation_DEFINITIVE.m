% Gaussian process reconstruction simulations

%addpath /home/lucamb/Matlab/Material
%addpath /home/lucamb/Matlab/fieldtrip-20150923

trl_max = 5;

%% Data generation
[Y_st,t, ind_center, ind_side] = ct_spatiotemporal_neural_data_DEFINITIVE(trl_max,0,0.2,1);

%% Data analysis (GP and Harmony)

[mean_pos, H_mean_pos, W_tot] = ct_GaussianAnalysis_simulations_DEFINITIVE(Y_st,t,5);

%% Spectral analysis
num_trl = size(Y_st,2);

omega_c = 2*pi*10;
basis = exp(1i*omega_c*t)'/numel(t);

W_s = single(W_tot)';

F_harmony_1 = zeros(size(W_s,1),num_trl);
F_harmony_2 = zeros(size(W_s,1),num_trl);

F_GP_1 = zeros(size(W_s,1),num_trl);
F_GP_2 = zeros(size(W_s,1),num_trl);

df = 1;
dps_seq = dpss(length(t),df*2);
basis_tp = dps_seq.*repmat(basis,1,size(dps_seq,2));

% Harmony
for trial = 1:size(Y_st,2)
    F_harmony_1(:,trial) = sqrt(mean(abs(((W_s*(H_mean_pos{trial,1}*basis_tp)))).^2,2));
    F_harmony_2(:,trial) = sqrt(mean(abs(((W_s*(H_mean_pos{trial,2}*basis_tp)))).^2,2));
    trial
end

%% GP
trl_max = size(Y_st,2);
for trial = 1:size(Y_st,2)
    F_GP_1(:,trial) = sqrt(sum((W_s*(mean_pos{trial,2,1}*mean_pos{trial,2,1}')).*W_s,2));
    F_GP_2(:,trial) = sqrt(sum((W_s*(mean_pos{trial,2,2}*mean_pos{trial,2,2}')).*W_s,2));
    trial
end

%% Effect size maps
trl_ind = 1:1;

effect_GP = (mean(ct_vect_to_scal(F_GP_1(:,trl_ind),'magnitude'),2) - mean(ct_vect_to_scal(F_GP_2(:,trl_ind),'magnitude'),2));
std_GP = (std(ct_vect_to_scal(F_GP_1(:,trl_ind),'magnitude')')' + std(ct_vect_to_scal(F_GP_2(:,trl_ind),'magnitude')')');
effect_H = (mean(ct_vect_to_scal(F_harmony_1(:,trl_ind),'magnitude'),2) - mean(ct_vect_to_scal(F_harmony_2(:,trl_ind),'magnitude'),2));
std_H = (std(ct_vect_to_scal(F_harmony_1(:,trl_ind),'magnitude')')' + std(ct_vect_to_scal(F_harmony_2(:,trl_ind),'magnitude')')');

scaling_GP = (mean(ct_vect_to_scal(F_GP_1(:,trl_ind),'magnitude'),2) + mean(ct_vect_to_scal(F_GP_2(:,trl_ind),'magnitude'),2));
scaling_H = (mean(ct_vect_to_scal(F_harmony_1(:,trl_ind),'magnitude'),2) + mean(ct_vect_to_scal(F_harmony_2(:,trl_ind),'magnitude'),2));


%% Focusing index and accuracy indices
focusing_GP = zeros(trl_max,1);
focusing_H = zeros(trl_max,1);
accuracy_GP = zeros(trl_max,1);
accuracy_H = zeros(trl_max,1);

for j = 1:trl_max
    s_GP_1= ct_vect_to_scal(F_GP_1(:,j),'magnitude');
    s_GP_2= ct_vect_to_scal(F_GP_2(:,j),'magnitude');
    
    center_GP = mean(abs((mean(s_GP_1(ind_center{1}),2) - mean(s_GP_2(ind_center{1}),2))));
    side_GP = mean(abs((mean(s_GP_1(ind_side{1}),2) - mean(s_GP_2(ind_side{1}),2))));
    center_GP2 = mean(abs((mean(s_GP_1(ind_center{2}),2) - mean(s_GP_2(ind_center{2}),2))));
    center_GP3 = mean(abs((mean(s_GP_1(ind_center{3}),2) - mean(s_GP_2(ind_center{3}),2))));
    focusing_GP(j) = center_GP/side_GP;
    accuracy_GP(j) = center_GP/max(center_GP2,center_GP3);
    
    % Harmony
    s_H_1= ct_vect_to_scal(F_harmony_1(:,j),'magnitude');
    s_H_2= ct_vect_to_scal(F_harmony_2(:,j),'magnitude');
    
    center_H = mean(abs((mean(s_H_1(ind_center{1}),2) - mean(s_H_2(ind_center{1}),2))));
    side_H = mean(abs((mean(s_H_1(ind_side{1}),2) - mean(s_H_2(ind_side{1}),2))));
    center_H2 = mean(abs((mean(s_H_1(ind_center{2}),2) - mean(s_H_2(ind_center{2}),2))));
    center_H3 = mean(abs((mean(s_H_1(ind_center{3}),2) - mean(s_H_2(ind_center{3}),2))));
    focusing_H(j) = center_H/side_H;
    accuracy_H(j) = center_H/max(center_H2,center_H3);
end

%% Outliers detection
dev_a_GP = abs(accuracy_GP - median(accuracy_GP));
dev_a_H = abs(accuracy_H - median(accuracy_H));

dev_a = max(dev_a_GP, dev_a_H);

dev_f_GP = abs(focusing_GP - median(focusing_GP));
dev_f_H = abs(focusing_H - median(focusing_H));

dev_f = max(dev_f_GP, dev_f_H);

ind_in_a = find(dev_a<4*median(dev_a));
ind_in_f = find(dev_f<4*median(dev_f));

a_GP = accuracy_GP(ind_in_a);
a_H = accuracy_H(ind_in_a);

f_GP = focusing_GP(ind_in_f);
f_H = focusing_H(ind_in_f);

%% Plot GP effect size
%close all

% figure, 
% ft_plot_mesh(sourcespace, 'vertexcolor', (effect_GP)./norm(effect_GP))
% caxis([-0.07, 0])
% 
% figure, 
% ft_plot_mesh(sourcespace, 'vertexcolor', (effect_H)./norm(effect_H))
% caxis([-0.07, 0])

a_min = min([a_GP;a_H]);
a_max = max([a_GP;a_H]);

f_min = min([f_GP;f_H]);
f_max = max([f_GP;f_H]);

figure, 
scatter((a_GP), (a_H),'r')
title('Accuracy')
axis square
xlim([a_min - 1, a_max + 1])
ylim([a_min - 1, a_max + 1])
refline(1,0)
title('Accuracy')
xlabel('Gaussian process')
ylabel('Harmony with multitapers')


figure, 
scatter(f_GP,f_H,'r')
title('Focusing')
axis square 
xlim([f_min - 1, f_max + 1])
ylim([f_min - 1, f_max + 1])
refline(1,0)
title('Focusing scatter plot')
xlabel('Gaussian process')
ylabel('Harmony with multitapers')


