% Spatiotemporal Harmonic GP source reconstruction with custum kernels
function [mean_pos, H_mean_pos, W_tot] = ct_GaussianAnalysis_simulations_DEFINITIVE(Y,time,a)
close all
clc


addpath /home/lucamb/Matlab/fieldtrip-20150923
addpath /home/lucamb/Matlab/Material
ft_defaults


num_ch = 274; %number of channels

%% Loading anatomical data (Standard mesh and mri scan)
load('Template_Anatomical_Data/Harmonic_material.mat');
load('Template_Anatomical_Data/Leadfield_syn_matrix.mat')


%% Generation of the basis function
load('Template_Anatomical_Data/Sphere_material.mat');

% Generating the basis function matrix
cfg_bf = [];
cfg_bf.basis_functions = 'spherical harmonics'; %This method combines spherical harmonics and spherical (mexhat) wavelets
cfg_bf.sph_harmonics.l_max = 12; %Maximal spatial frequency of the spherical harmonics basis functions
%cfg_bf.sph_wavelets.scales = 11; %vector of 'scales' of the spherical wavelets
%cfg_bf.sph_wavelets.number = 9; %The number of wavelets per scales is number*k

[W_tot, freq_tot] = ct_basis_functions(cfg_bf, sphere_material);

%% Import data
y = cell(2*size(Y,2),1);

for j = 1:2*size(Y,2)
    if j<=size(Y,2)
        y{j} = Y{1,j};
    else
        y{j} = Y{2,j-size(Y,2)};
    end
end

%% PCA
num_pc = 5;
num_trl_pca = min(50,length(y));
concX = [];
for z = 1:num_trl_pca
    concX = [concX, y{z}];
end

[U,D,V] = svd(concX,'econ');

U_red = U(:,1:num_pc)';

%%
num_trl = length(y);
c = 0;
for j = 1:num_trl;
    z = U_red*y{j};
    c = c + cov(z);
    j
end



c_avg = c/num_trl;
max_c = max(max(c_avg));
c_avg = c_avg/max_c;

% stationarization
c_stat = 0;
for j = -length(time):length(time)
    c_stat = c_stat + diag(mean(diag(c_avg,j))*ones(length(diag(c_avg,j)),1),j);
end

figure,
plot(diag(fliplr(c_stat)))
%ylim([-1 1])


%% Harmony regularization parameter (Cross validation)

B = [];
max_trl_CV = 5;
for t = 1:max_trl_CV
    B = [B, y{t}];
end

G_h = G_tot_syn*W_tot'; % leadfield in the harmonic base;

l_c = 3; % Stop-band
k = 2; % Order

SIG_X = eye(size(W_tot,1));
for j = 1:size(W_tot,1)
    %SIG_X(j,j) = exp(-freq_tot(j).^2./(2*p^2)); %Gaussian
    SIG_X(j,j) = 1/sqrt(1 + (freq_tot(j)/l_c).^(2*k)); % Butterworth
end

log_lambda = -26:0.25:-5;
cost = zeros(length(log_lambda),1);

for j = 1:length(log_lambda)
    cost(j) = ct_CV_LoO(log_lambda(j),B, G_h, SIG_X);
    clc
    disp(num2str(j))
end

figure,
plot(log_lambda, log(cost))

[min_l, ind_l] = min(cost);
% 
lambda_space = 10^log_lambda(ind_l);

SIG_Y = lambda_space*eye(size(G_h,1));
P_H = (SIG_X*G_h')/(SIG_Y + G_h*SIG_X*G_h'); %projection matrix;



%% Prior parameters
[T_x T_y] = meshgrid(time,time); % For constructing the kernel

% Available priors:
% ct_prior_logNorm (Log-normal distribution)
%ct_prior_unif (Uniform distribution)

% Kernel configuration
num_kernels = 3; % Number of kernels
kernels = {'exponential','damped_harmonic_oscillator_alpha', 'squared_exponential'}; % Type of kernels

% Prior configuration
cfg_param = [];
num_param = [ 2, 3, 2];
% Each hyper-parameter can have its own (hyper-)prior distribution
prior_pdf{1} = {'logNorm', 'unif'}; %Exponential
prior_pdf{2} = {'logNorm', 'logNorm', 'beta'};
prior_pdf{3} = {'logNorm','unif'};


% Hyper-prior parameters
prior_hyp_parameters{1} = { [ 1, 2], [0, 5]}; %Exponential
prior_hyp_parameters{2} = { [ 1, 2], [0.01, 40], [2*pi*8, 2*pi*13, 1, 1]}; %Alpha 
prior_hyp_parameters{3} = { [ 1, 2], [ 0, 0.03]}; 

% Variable transformations
var_transform{1} = {'log', 'logit'};
var_transform{2} = {'log','log', 'logit' };
var_transform{3} = {'log','logit'};

transform_param{1} = {[], [0, 5]};
transform_param{2} = {[], [], [2*pi*8, 2*pi*13] };
transform_param{3} = {[], [0, 0.03]};

for j = 1:num_kernels
    % Parameters structure
    cfg = [];
    cfg.numparam = num_param(j); % number of (hyper-)parameters
    cfg.pdf = prior_pdf{j};
    cfg.hyparam = prior_hyp_parameters{j};
    cfg.tran = var_transform{j};
    cfg.tranparam = transform_param{j}; 
    cfg_param = setfield(cfg_param, kernels{j}, cfg);
end

% Kernel structure
cfg_k = [];
cfg_k.kernels = kernels;
cfg_k.num = num_kernels;
cfg_k.num_param = max(num_param);

% Parameters initialization
in_param = [log(1/4), log(1), log(1/4), 0, 0, log(1/4), 0  ]'  + 0.1*randn(sum(num_param),1);
                  
%% Simulated annealing 
% Finding the global mode that will be also used as a starting point for
% the monte carlo sampling
sigma = 0.1;

l = @(x) ct_least_squares4(cfg_k, cfg_param, x, c_stat, T_x, T_y);
g = @(x) x + randn(1)*sigma*mnrnd(1,ones(size(x))/length(x)).*trnd(2);

cfg = [];
cfg.Verbosity = 2;
cfg.StopTemp = 10^-a;
cfg.Generator = g;
cfg.InitTemp = 100;
mode = anneal(l,in_param',cfg);

lambda_n = 0.1;

%% Plot
theta = ct_transform(cfg_k, cfg_param, mode);
[K_tot K] = ct_kernel(cfg_k, theta, T_x, T_y, lambda_n);
K_tot = max_c*K_tot;
K = max_c*K;

tau = (T_x - T_y);
k_plot = [ fliplr(K_tot(2:end,1)'), K_tot(1,1:end)];
c_plot = [ fliplr(c_stat(2:end,1)'), c_stat(1,1:end)];
t_plot = [ fliplr(tau(2:end,1)'), tau(1,1:end)];

figure, 
plot(t_plot, max_c*c_plot, t_plot, k_plot)
legend('Data autocovariance','Parametric fitting')

%% Channel posterior 
% ind_loc_l = 4518;
% ind_loc_r = 13496;

trl_max = length(y)/2;

mean_pos = cell(trl_max,cfg_k.num,2);

[K_tot, K] = ct_kernel(cfg_k, ct_transform(cfg_k, cfg_param, mode), T_x, T_y, lambda_n);
for h = 1:2
    for trial = 1:trl_max
        for k = 1:cfg_k.num
            inv_K = (squeeze((K(k,:,:)))/(K_tot));
            mean_pos{trial,k,h} = (Y{h,trial})*inv_K';
            clc
            disp(['Posterior computation, ',' Kernel: ', num2str(k), ' Trial: ', num2str(trial), ' Condition: ', num2str(h)])
        end
    end
end

%trl_max = length(data_clean.trial);

%% GP regularization parameter (Cross validation)

B = [];
max_trl_CV = 5;
for t = 1:max_trl_CV
    B = [B, mean_pos{t,2,1}];
end

l_c = 3; % Stop-band
k = 2; % Order

SIG_X = eye(size(W_tot,1));
for j = 1:size(W_tot,1)
    %SIG_X(j,j) = exp(-freq_tot(j).^2./(2*p^2)); %Gaussian
    SIG_X(j,j) = 1/sqrt(1 + (freq_tot(j)/l_c).^(2*k)); % Butterworth
end

log_lambda = -26:0.25:-5;
cost = zeros(length(log_lambda),1);

for j = 1:length(log_lambda)
    cost(j) = ct_CV_LoO(log_lambda(j),B, G_h, SIG_X);
    clc
    disp(num2str(j))
end

figure,
plot(log_lambda, log(cost))

[min_l, ind_l] = min(cost);
% 
lambda_GP = 10^log_lambda(ind_l);

SIG_Y = lambda_GP*eye(size(G_h,1));
P_GP = (SIG_X*G_h')/(SIG_Y + G_h*SIG_X*G_h'); %GP projection matrix;

%% Space posterior

for h = 1:2
    for trial = 1:trl_max
        for k = 1:cfg_k.num
            inv_K = (squeeze((K(k,:,:)))/(K_tot));
            mean_pos{trial,k,h} = P_GP*mean_pos{trial,k,h};
            clc
            disp(['Posterior computation, ',' Kernel: ', num2str(k), ' Trial: ', num2str(trial), ' Condition: ', num2str(h)])
        end
    end
end

%% Harmony
H_mean_pos = cell(trl_max,2);
for h = 1:2
    for trial = 1:trl_max
        H_mean_pos{trial,h} = P_H*Y{h,trial};
    end
end

%% Figures









