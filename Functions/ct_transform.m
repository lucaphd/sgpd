% variable transform

function theta = ct_transform(cfg_k,cfg_param,x)
theta = cell(cfg_k.num,1);
in = 0;
for j = 1:cfg_k.num
    for k = 1:cfg_param.(cfg_k.kernels{j}).numparam
        if strcmp(cfg_param.(cfg_k.kernels{j}).tran{k},'log')  %Logarithmic transform
            theta{j}(k) = exp(x(in + k));    
        elseif strcmp(cfg_param.(cfg_k.kernels{j}).tran{k},'logit') %Logit transform
            a = cfg_param.(cfg_k.kernels{j}).tranparam{k}(1); %Lower bound
            b = cfg_param.(cfg_k.kernels{j}).tranparam{k}(2); %Upper bound
            theta{j}(k) = (b - a)*logsig(x(in + k)) + a;
        else
            error('Unknow transformation');
        end
    end
    in = in + k;
end
    