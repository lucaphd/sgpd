% Kernel
function [ K_tot, K ] = ct_kernel(cfg, theta, T_x, T_y, lambda_n)

tau = T_x - T_y;
h = abs(tau(:,1));
K = zeros(cfg.num,size(tau,1),size(tau,2));
for j = 1:cfg.num
    if isempty(strfind(cfg.kernels{j}, 'periodic')) == 0 % STandard periodic Kernel 
        %% Parameters
        SD = theta{j}(1); %Amplitude
        omega = theta{j}(2); %Frequency
        rho = theta{j}(3); %Roughness
        %% Kernel
        K_tmp = SD.^2*exp(- 2*sin((omega*h)/2).^2/rho.^2);
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'quasi_periodic_SqExp')) == 0; % Quasi periodic Kernel with gaussian decay
        %% Parameters
        SD = theta{j}(1); %Amplitude
        l =  theta{j}(2); %Forgetting time scale
        omega = theta{j}(3); %Frequency
        rho = theta{j}(4); %Roughness
        %% Kernel
        K_tmp = SD.^2*exp(-h.^2/(2*l.^2) - 2*sin((omega*h)/2).^2/rho.^2);
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'damped_harmonic_oscillator')) == 0 % Damped harmonic oscillator
        %% Parameters
        SD = theta{j}(1); %Amplitude
        b =  theta{j}(2); %Damping parameter
        omega = theta{j}(3); %Frequency
        %% Kernel
        K_tmp = SD.^2*exp(-abs(h)*b).*(cos(omega*h) + (b/omega)*sin(omega*abs(h)));
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'second_order_integrator')) == 0 % Second order integrator
        %% Parameters
        SD = theta{j}(1); %Amplitude
        b =  theta{j}(2); 
        a = theta{j}(3); 
        %% Kernel
        K_tmp = SD.^2*exp(-abs(h)*b).*(cosh(a*h) + (b/a)*sinh(a*abs(h)));
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'harmonic_with_fAR1_decay')) == 0 % Fractional Ornstein-Uhlenbeck Kernel
        %% Parameters
        SD = theta{j}(1); %Amplitude
        a = theta{j}(2); %Relaxation parameter
        H = theta{j}(3); %Hurst exponent
        omega = theta{j}(4); %Frequency
        
        %% Pre-computations
        lambda = -a; %Eigenvalue
        dH = 1 - 2*H;
        G_p = gammainc(lambda*abs(h),2*H);
        G_n = gammainc(-lambda*abs(h),2*H);
        %% Kernel
        u = real(2*(-lambda).^dH.*cosh(lambda*abs(h)) + lambda.^dH*exp(lambda*abs(h)).*G_p - (-lambda).^dH*exp(-lambda*abs(h)).*G_n);
        K(j,:,:) = ct_stationary_kernel(SD.^2*u/(2*(-lambda).^dH).*cos(omega*h)); 
    elseif isempty(strfind(cfg.kernels{j}, 'waxing_and_waning')) == 0 %Waxing and waning
        %% Parameters
        SD = theta{j}(1); %Amplitude
        b =  theta{j}(2); %Damping parameter
        omega_fast = theta{j}(3); %Carrier frequency
        omega_slow = theta{j}(4); %Slow frequency
        rho = theta{j}(5); %Roughness of the envelope
        
        %% Kernel
        K_tmp = SD.^2*exp(-abs(h)*b).*(cos(omega_fast*h) + (b/omega_fast)*sin(omega_fast*abs(h))).*exp(- 2*sin((omega_slow*h)/2).^2/rho.^2);
        K(j,:,:) = ct_stationary_kernel(K_tmp);
        
    elseif isempty(strfind(cfg.kernels{j}, 'harmonic_oscillator')) == 0 % Harmonic oscillator
        %% Parameters
        SD = theta{j}(1); %Amplitude
        omega = theta{j}(2); %Frequency
        %% Kernel
        K_tmp = SD.^2*cos(omega*h);
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'quasi_periodic_RQ')) == 0 % Quasi periodic Kernel with RQ decay
        %% Parameters
        SD = theta{j}(1); %Amplitude
        l =  theta{j}(2); %Forgetting time scale
        k =  theta{j}(3); %Scale mixture
        omega = theta{j}(4); %Frequency
        rho = theta{j}(5); %Roughness
        %% Kernel
        K_tmp = SD.^2*((1 + h.^2./(2*k*l^2)).^(-k)).*exp(-2*sin((omega*h)/2).^2/rho.^2);
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'rational_quasi_periodic_RQ')) == 0 % Rational quasi periodic Kernel with RQ decay
        %% Parameters
        SD = theta{j}(1); %Amplitude
        l =  theta{j}(2); %Forgetting time scale
        k =  theta{j}(3); %Scale mixture (forgetting)
        omega = theta{j}(4); %Frequency
        rho = theta{j}(5); %Roughness
        nu = theta{j}(6); %Scale mixture (roughness)
        %% Kernel
        K_tmp = SD.^2*((1 + h.^2./(2*k*l^2)).^(-k)).*(1 + (2/(rho^2*nu))*(1 - cos(omega*h))).^-nu;
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'fractional_AR1')) == 0 % Fractional Ornstein-Uhlenbeck Kernel
        %% Parameters
        SD = theta{j}(1); %Amplitude
        a = theta{j}(2); %Relaxation parameter
        H = theta{j}(3); %Hurst exponent
        
        %% Pre-computations
        lambda = -a; %Eigenvalue
        dH = 1 - 2*H;
        G_p = gammainc(lambda*abs(h),2*H);
        G_n = gammainc(-lambda*abs(h),2*H);
        %% Kernel
        u = real(2*(-lambda).^dH.*cosh(lambda*abs(h)) + lambda.^dH*exp(lambda*abs(h)).*G_p - (-lambda).^dH*exp(-lambda*abs(h)).*G_n);
        K(j,:,:) = ct_stationary_kernel(SD.^2*u/(2*(-lambda).^dH)); 
    elseif isempty(strfind(cfg.kernels{j}, 'squared_exponential')) == 0 % Squared exponential Kernel
        %% Parameters
        SD = theta{j}(1); %Amplitude
        l =  theta{j}(2); %Time scale
        %% Kernel
        K_tmp = SD.^2*exp(-h.^2/(2*l.^2));
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'exponential')) == 0 % Squared exponential Kernel
        %% Parameters
        SD = theta{j}(1); %Amplitude
        l =  theta{j}(2); %Time scale
        %% Kernel
        K_tmp = SD.^2*exp(-abs(h)/l);
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'rational_quadratic')) == 0 % Rational quadratic Kernel
        %% Parameters
        SD = theta{j}(1); %Amplitude
        l =  theta{j}(2); %Forgetting time scale
        k = theta{j}(3); %Scale mixture 
        %% Kernel
        K_tmp = SD^2.*(1 + h.^2./(2*k*l^2)).^(-k);
        K(j,:,:) = ct_stationary_kernel(K_tmp);
    elseif isempty(strfind(cfg.kernels{j}, 'linear')) == 0  % Linear kernel
        %% Parameters
        SD = theta{j}(1); %Amplitude
        c = theta{j}(2); %Intercept
        %% Kernel
        K_tmp = SD^2.*(c + T_x.*T_y);
        K(j,:,:) = K_tmp;
    elseif isempty(strfind(cfg.kernels{j}, 'constant')) == 0  % Constant kernel
        %% Parameters
        SD = theta{j}(1); %Amplitude
        %% Kernel
        K_tmp = SD.^2*ones(size(tau));
        K(j,:,:) = K_tmp;
    elseif isempty(strfind(cfg.kernels{j}, 'white_noise')) == 0  % Linear kernel
        %% Parameters
        SD = theta{j}(1); %Amplitude
        %% Kernel
        K_tmp = SD^2*eye(size(tau));
        K(j,:,:) = K_tmp;
    else
        error('Unknown Kernel')
    end
end

K_tot = squeeze(sum(K,1)) + lambda_n*eye(size(tau));