%% Marginal likelihood (complex oscillator)

function [log_ml,L] = ct_log_marginal_likelihood(signal,K)

signal = signal(:);

% Kernel inversion and determinant
L = chol(K);
white_signal = L\signal;
log_det_K = 2*sum(log(diag(L)));

% Log marginal likelihood
log_ml = -length(signal)/2*log(2*pi) -(1/2)*log_det_K -(1/2)*(white_signal'*white_signal);