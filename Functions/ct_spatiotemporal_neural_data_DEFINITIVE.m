% Neural signal detection simulations
function [Y_st,t,ind_center,ind_side] = ct_spatiotemporal_neural_data_DEFINITIVE(N,gauss_a,sig,noise_r)
%close all

%kov = 0;
%gauss_a = 0;

%% Import
% Loading anatomical data (Standard mesh and mri scan)
path = 'Template_Anatomical_Data/Harmonic_material.mat';
load(path);
load('Template_Anatomical_Data/Leadfield_syn_matrix.mat')


% Generation of the basis function
path = 'Template_Anatomical_Data/Sphere_material.mat';
load(path);

% Generating the basis function matrix
cfg_bf = [];
cfg_bf.basis_functions = 'spherical harmonics'; %This method combines spherical harmonics and spherical (mexhat) wavelets
cfg_bf.sph_harmonics.l_max = 12; %Maximal spatial frequency of the spherical harmonics basis functions
%cfg_bf.sph_wavelets.scales = 11; %vector of 'scales' of the spherical wavelets
%cfg_bf.sph_wavelets.number = 9; %The number of wavelets per scales is number*k

[W_tot, freq_tot] = ct_basis_functions(cfg_bf, sphere_material);

G_h = G_tot_syn*W_tot'; % leadfield in the harmonic base

%% Constants
dt = 1/300;
T = 1.3;
t = 0:dt:T;
t_test_ind = find(t>0.3&t<1);
%N = 60;

[T_x T_y] = meshgrid(t,t); % For constructing the kernel
signal = cell(2,1);
alpha = cell(2,N);
broad = cell(2,1);
pink = cell(2,1);

mu_amp = [0, sig]; %signal amplitude

Y_st = cell(2,N);
S_st = cell(2,N);

%% Spatial signal
% Generation of the basis function
path = '/vol/actper-scratch1/lucamb/Harmonics Material/Sphere_material.mat';
load(path);

% Generating the basis function matrix
cfg_bf = [];
cfg_bf.basis_functions = 'spherical mexhat wavelets'; %This method combines spherical harmonics and spherical (mexhat) wavelets
%cfg_bf.sph_harmonics.l_max = 12; %Maximal spatial frequency of the spherical harmonics basis functions
cfg_bf.sph_wavelets.scales = [2 9]; %vector of 'scales' of the spherical wavelets
cfg_bf.sph_wavelets.number = 9; %The number of wavelets per scales is number*k

[C_tot, freq_tot] = ct_basis_functions(cfg_bf, sphere_material);

    %% Wavelet selection
 v1 = 181;
v2 = 16;
v3 = 158;
%[S] = ct_vect_to_scal(C_tot(v3,:)','magnitude');
%figure, 
%ft_plot_mesh(sourcespace, 'vertexcolor', S);

for n = 1:N

    %% Temporal signals

    for j = 1:2
        %% Non-linear alpha amplitude process
        % covariance function
        amp_1 = 0.01; %variance
        nu_1 = 0.2; % length scale
        K_amp = amp_1.^2*exp(-(T_x-T_y).^2/nu_1);
        % mean function
        m_1 = 0.3;

        %nu_m = 0.5; %signal width
        %c_m = 0.7; %signal center
        mean_amp = m_1*ones(length(t),1) + mu_amp(j); %*normpdf(t,c_m,nu_m);

        r1s = mvnrnd(mean_amp,K_amp,1);
        r1 = mvnrnd(m_1*ones(length(t),1),K_amp,2);
        p1 = sqrt(1+r1.^2) - 1;
        p1s = sqrt(1+r1s.^2) - 1;
        %figure, plot(t, p1)

        %% Non-stationary oscillatory process
        omega = ones(length(t),1)*10*2*pi;
        sd_2 = 3;
        nu_2 = 0.2; % frequency length scale
        K_freq = sd_2.^2*exp(-(T_x-T_y).^2/nu_2);

        r2 = mvnrnd(omega,K_freq,3);
        p2 = cos(r2.*repmat(t,3,1) + 2*pi*repmat(rand(3,1),1,length(t)));
        %figure, plot(t, p2)

        %% Alpha oscillations

        if gauss_a == 1
            amp_1 = 0.1; %variance
            nu_1 = 0.05; % length scale
            K_alpha = (amp_1+mu_amp(j)).^2*exp(-(T_x-T_y).^2/nu_1).*cos(2*pi*10*abs(T_x-T_y));
            % mean function
            m_1 = 1;
            %nu_m = 0.5; %signal width
            %c_m = 0.7; %signal center
            mean_amp = m_1*ones(length(t),1) + mu_amp(j); %*normpdf(t,c_m,nu_m);
            p3 = mvnrnd(zeros(size(t))',K_alpha,3);
        else
           p3 = p1(1:2,:).*p2(1:2,:);
           p3s = p1s.*p2(3,:);
           alpha{j,n} = [p3s; p3];
        end

        %% Broadband noise
        % covariance function
        amp_4 = 0.05; %variance
        nu_4 = dt/8; % length scale
        K_bn = amp_4.^2*exp(-(T_x-T_y).^2/nu_4);

        p4 = mvnrnd(zeros(length(t),1),K_bn,3);
        broad{j,n} = p4;
        %figure, plot(t, p4)

        %% Pink noise
        % covariance function
        amp_5 = 0.05; %variance
        k_5 = 0.2; % length scale
        K_bn = amp_5.^2*exp(-abs(T_x-T_y)/k_5);

        p5 = mvnrnd(zeros(length(t),1),K_bn,3);
        pink{j,n} = p5;
        %figure, plot(t, p5)

        %% Total noisy signal

        %signal{j} = p3 + p4 + p5;
        %figure, plot(t, signal(1,:))

        %% Spectra
        %s3 = pwelch(p3');
        %s4 = pwelch(p4');
        %s5 = pwelch(p5');
    end

    %%
    C_h1 = C_tot(v1,:)*W_tot';
    C_h2 = noise_r*C_tot(v2,:)*W_tot';
    C_h3 = noise_r*C_tot(v3,:)*W_tot';

    sigma = 0.05*10^-5;

    S_st{1,n} = C_h1'*(alpha{1,n}(1,:)+ broad{1,n}(1,:) + pink{1,n}(1,:)) + C_h2'*(alpha{1,n}(2,:)+ broad{1,n}(2,:) + pink{1,n}(2,:)) +C_h3'*(alpha{1,n}(3,:)+ broad{1,n}(3,:) + pink{1,n}(3,:)) ;
    S_st{2,n} = C_h1'*(alpha{2,n}(1,:)+ broad{2,n}(1,:) + pink{2,n}(1,:)) + C_h2'*(alpha{2,n}(2,:)+ broad{2,n}(2,:) + pink{2,n}(2,:)) +C_h3'*(alpha{2,n}(3,:)+ broad{2,n}(3,:) + pink{2,n}(3,:)) ;
    
    Y_st{1,n} = G_h*S_st{1,n} + sigma*randn(size(G_h,1),length(t));
    Y_st{2,n} = G_h*S_st{2,n} + sigma*randn(size(G_h,1),length(t));
    n
end


%% Center and side

ind_center = cell(3,1);
ind_side = cell(3,1);


w1 = ct_vect_to_scal((C_h1*W_tot)','magnitude');

[srt_effect srt_ind_1] = sort(abs(w1),'descend');

ind_center{1} = srt_ind_1(1:50);
ind_side{1} = srt_ind_1(51:end);

w2 = ct_vect_to_scal((C_h2*W_tot)','magnitude');

[srt_effect srt_ind_2] = sort(abs(w2),'descend');

ind_center{2} = srt_ind_2(1:50);
ind_side{2} = srt_ind_2(51:end);

w3 = ct_vect_to_scal((C_h3*W_tot)','magnitude');

[srt_effect srt_ind_3] = sort(abs(w3),'descend');

ind_center{3} = srt_ind_3(1:50);
ind_side{3} = srt_ind_3(51:end);

%% Plot effect

%figure, 
%ft_plot_mesh(sourcespace, 'vertexcolor', abs(w1))




















