% From vector fields to mean scalars

function [S] = ct_vect_to_scal(V,str,varargin)

if ndims(V) == 1
    V_x = V(1:end/3);
    V_y = V(end/3 + 1:(2/3)*end);
    V_z = V((2/3)*end + 1:end);
elseif ndims(V) == 2
    V_x = V(1:end/3,:);
    V_y = V(end/3 + 1:(2/3)*end,:);
    V_z = V((2/3)*end + 1:end,:);
elseif ndims(V) == 3
    V_x = V(1:end/3,:,:);
    V_y = V(end/3 + 1:(2/3)*end,:,:);
    V_z = V((2/3)*end + 1:end,:,:);
else
    error('arrays with more than 3 "dimensions" are not supported')
end

if strcmp(str,'abs_mean')
    S = (1/3)*(abs(V_x) + abs(V_y) + abs(V_z));
elseif strcmp(str,'magnitude')
    S = sqrt(abs(V_x).^2 + abs(V_y).^2 + abs(V_z).^2);
elseif strcmp(str,'max')
    S = max(max(V_x,V_y),V_z);
elseif strcmp(str,'abs_sum')
    S = (abs(V_x) + abs(V_y) + abs(V_z));
elseif strcmp(str,'sum')
    S = ((V_x) + (V_y) + (V_z));
elseif strcmp(str,'svd')
    for j = 1:size(V(1:end/3,:),1)
        num_dat = varargin{1};
        Dat = detrend(real([ V(j,1:num_dat);V(end/3 + j,1:num_dat); V(end*(2/3) + j,1:num_dat)])','constant');
        [A,~,~] = svd(Dat,'econ');
        u = A(:,1);
        S = u(1)*V_x + u(2)*V_y + u(3)*V_z;
        disp(num2str(j));
    end
else
    error('The required method is not implemented')
end