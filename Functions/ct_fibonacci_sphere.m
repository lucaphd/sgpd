% Fibonacci sphere

function [ grid ] = ct_fibonacci_sphere(P)

N = ceil((P - 1)/2);

grid = zeros(P,3);
g_ratio = (sqrt(5)+1)/2;

n = 1;
for l = -N:N
        theta = 2*pi*l/g_ratio;
        phi = asin(2*l/P);
        grid(n,1) = 1.1*cos(phi)*cos(theta);
        grid(n,2) = 1.1*cos(phi)*sin(theta);
        grid(n,3) = 1.1*sin(phi);
        n = n + 1;
end

end

