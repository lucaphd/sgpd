% Basis function generator (standard mesh)

function [W_tot, freq_tot, S_l, S_r] = ct_basis_functions(cfg, mesh_data)

% %% loading files (Standard mesh)
% surf_sphere_left = ft_read_headshape('/vol/dccdata/actper/lucamb/FreesurferMaterial/buckner_data/tutorial_subjs/good_output/surf/lh.sphere');
% surf_sphere_right = ft_read_headshape('/vol/dccdata/actper/lucamb/FreesurferMaterial/buckner_data/tutorial_subjs/good_output/surf/rh.sphere');
% bnd = ft_read_headshape('/vol/dccdata/actper/lucamb/FreesurferMaterial/TestData/good_output/bem/good_output-ico-5-src.fif', 'format', 'mne_source');
% surf_org_l = ft_read_headshape('/vol/dccdata/actper/lucamb/FreesurferMaterial/buckner_data/tutorial_subjs/good_output/surf/lh.white');
% surf_org_r = ft_read_headshape('/vol/dccdata/actper/lucamb/FreesurferMaterial/buckner_data/tutorial_subjs/good_output/surf/rh.white');
 
surf_sphere_left = mesh_data.surf_sphere_left;
surf_sphere_right = mesh_data.surf_sphere_right;
bnd = mesh_data.bnd;
surf_org_l = mesh_data.surf_org_l;
surf_org_r = mesh_data.surf_org_r;

%% Dividing the mesh into two separated hemospheres
L = size(bnd.pnt,1)/2;

surf_left = [];
surf_left.pnt = bnd.pnt(1: end/2,:);
surf_left.tri = bnd.tri(1: end/2,:);
surf_left.unit = bnd.unit;

surf_right = [];
surf_right.pnt = bnd.pnt(end/2 +1: end,:);
surf_right.tri = bnd.tri(end/2 +1: end,:) - L;
surf_right.unit = bnd.unit;

%% Generating spherical coordinates
ind_dwn_l = find(bnd.orig.inuse(1:size(surf_sphere_left.pnt,1)) == 1);
ind_dwn_r = find(bnd.orig.inuse(size(surf_sphere_left.pnt,1)+1:end,1) == 1);

for t = 1:length(ind_dwn_l)
    k_l = ind_dwn_l(t);
    [phi,theta,rho] = cart2sph(surf_sphere_left.pnt(k_l,1),surf_sphere_left.pnt(k_l,2),surf_sphere_left.pnt(k_l,3));
    surf_left.sphere(t,:) = [ phi + pi, theta + pi/2 ];
end

for t = 1:length(ind_dwn_r)
    k_r = ind_dwn_r(t);
    [phi,theta,rho] = cart2sph(surf_sphere_right.pnt(k_r,1),surf_sphere_right.pnt(k_r,2),surf_sphere_right.pnt(k_r,3));
    surf_right.sphere(t,:) = [ phi + pi, theta + pi/2 ];
end

%% Constructing basis functions
if strcmp(cfg.basis_functions,'spherical harmonics')
    %% Spherical harmonics
    l_max = cfg.sph_harmonics.l_max; %Highest spatial frequency number
    
    num_harm = (l_max + 1)^2; %Number of spherical harmonics (per direction and hamisphere)

    A_l = zeros(num_harm,size(surf_left.sphere,1));
    freq = zeros(num_harm,1);

    d = 1;
    k = 1;
    for l = 0:l_max
        m = -l:1:l;
        for j = 1:length(m)
            C = sqrt(((2*l + 1)*factorial(l - abs(m(j)))/(4*pi*factorial(l + abs(m(j))))));
            P = legendre(l,cos(surf_left.sphere(:,2)));
            if m(j) == 0
                A_l(k,:) = C.*P(abs(m(j)) + 1,:);
            elseif m(j) >= 0
                A_l(k,:) = C.*P(abs(m(j)) + 1,:).*sqrt(2).*cos(m(j).*surf_left.sphere(:,1))';
            else
                A_l(k,:) = C.*P(abs(m(j)) + 1,:).*sqrt(2).*sin(abs(m(j)).*surf_left.sphere(:,1))';
            end
            freq(d) = l;
            d = d + 1;
            k = k + 1;
        end
    end

    A_r = zeros(num_harm,size(surf_left.sphere,1));

    k = 1;
    for l = 0:l_max
        m = -l:1:l;
        for j = 1:length(m)
            C = sqrt(((2*l + 1)*factorial(l - abs(m(j)))/(4*pi*factorial(l + abs(m(j))))));
            P = legendre(l,cos(surf_right.sphere(:,2)));
            if m(j) == 0
                A_r(k,:) = C.*P(abs(m(j)) + 1,:);
            elseif m(j) >= 0
                A_r(k,:) = C.*P(abs(m(j)) + 1,:).*sqrt(2).*cos(m(j).*surf_right.sphere(:,1))';
            else
                A_r(k,:) = C.*P(abs(m(j)) + 1,:).*sqrt(2).*sin(abs(m(j)).*surf_right.sphere(:,1))';
            end
            freq(d) = l;
            d = d + 1;
            k = k + 1;
        end
    end
    % Saving left and right wavelets
    S_l = A_l;
    S_r = A_r;
    
    % Each hemisphere and dipole directio has its own set of harmonics
    A = blkdiag(A_l, A_r);
    W_tot = blkdiag(A, A, A);
    freq_tot = [freq; freq; freq];

elseif strcmp(cfg.basis_functions,'spherical mexhat wavelets')
    %% Spherical mexhat wavelets
    k_vec = cfg.sph_wavelets.scales; %Vector of scales
    num = cfg.sph_wavelets.number; %The number of wavelets per scales is num*k

    x = surf_sphere_left.pnt(ind_dwn_l,:)/100;
    W_l = [];

    for j = 1:length(k_vec)

        %Parameters
        k = k_vec(j);
        N = num*k;

        % Fibonacci spherical grid
        [ grid ] = ct_fibonacci_sphere(N); %This function generates (approximately) equally spaced points on a sphere

        % Spherical wavelets (mexhat)
        for n = 1:size(grid,1)
            mu = grid(n,:)';
            C = 3/(4*pi*sinh(k)); %normalization
            L = -C*(ct_d2_fisher(x, k, mu, 1) + ct_d2_fisher(x, k, mu, 2) + ct_d2_fisher(x, k, mu, 3));
            W_l = [W_l , L];
        end
    end

    x = surf_sphere_right.pnt(ind_dwn_r,:)/100;
    W_r = [];

    for j = 1:length(k_vec)

        %Parameters
        k = k_vec(j);
        N = num*k;

        % Fibonacci spherical grid
        [ grid ] = ct_fibonacci_sphere(N);

        % Spherical wavelets (mexhat)
        for n = 1:size(grid,1)
            mu = grid(n,:)';
            C = 3/(4*pi*sinh(k)); %normalization
            L = -C*(ct_d2_fisher(x, k, mu, 1) + ct_d2_fisher(x, k, mu, 2) + ct_d2_fisher(x, k, mu, 3));
            W_r = [W_r , L];
        end
    end
    
    % Saving left and right wavelets
    S_l = W_l;
    S_r = W_r;
    
    % Each hemisphere and dipole directio has its own set of wavelets
    W = blkdiag(W_l , W_r);
    W_tot = blkdiag(W, W, W)';
    freq_tot = [];
elseif strcmp(cfg.basis_functions,'combined')
    cfg_tmp = cfg;
    cfg_tmp.basis_functions = 'spherical harmonics';
    [A_tot, freq_tot, A_l, A_r] = ct_basis_functions(cfg_tmp, mesh_data);
    cfg_tmp.basis_functions = 'spherical mexhat wavelets';
    [W_tot, w_freq_tot, W_l, W_r] = ct_basis_functions(cfg_tmp, mesh_data);
    
    S_l = [W_l, A_l'];
    S_r = [W_r, A_r'];
    
    % Constructing the combined wavelets/harmonics matrix
    W = blkdiag( S_l, S_r);
    W_tot = blkdiag(W, W, W)';  
else
    error('The required set of basis functions is not currently implemented')
end


