% second derivative of a van Mises-Fisher pdf

function [ f ] = ct_d2_fisher(x, k, mu, n)

f = exp(k*x*mu).*((k*mu(n) - k*x(:,n).*(x*mu)).^2 + 3*k*x(:,n).^2.*(x*mu) - k*x*mu - 2*k*mu(n)*x(:,n));

end