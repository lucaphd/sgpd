function [K] = ct_stationary_kernel(k)
k = k(:);
N = length(k);
K = zeros(N);
for n = 1:N
    K(:,n) = [flipud(k(2:n)); k(1:N-n + 1)];
end
