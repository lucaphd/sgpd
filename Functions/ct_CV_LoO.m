% Cross-validation leave-one-out

function [ c ] = ct_CV_LoO(log_lambda,B, G_h, SIG_X)

I = eye(size(B,1));
P = (SIG_X*G_h')/(10^(log_lambda)*I + G_h*SIG_X*G_h'); %Spatial projection matrix
num = (B - G_h*P*B);
tmp_den = sum(G_h.*P',2);
den = 1 - repmat(tmp_den,1,size(B,2));
c = sum(sum((num./den).^2)); %Cost function for lambda_j

