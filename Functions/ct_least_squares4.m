% Least squares

function [ C ] = ct_least_squares4(cfg_k, cfg_param, x, c_avg, T_x, T_y)
A = ct_transform(cfg_k, cfg_param, x);
K_tot  = ct_kernel(cfg_k, A, T_x, T_y, 0);
C = sum(sum((c_avg - K_tot).^2));