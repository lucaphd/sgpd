% Decomposition comparison
close all
clc

%% Signal generation

% Constants
dt = 0.02;
T = 1;
t = 0:dt:0.8;
t_test_ind = find(t>0.1&t<0.5);
N = 1;
number_trials = 600;
number_time_points = length(t);
[T_x, T_y] = meshgrid(t,t); % For constructing the kernel
exponent = 1.;
theta_frequency = 8; % 5 or 8
dx = 0.5;
spatial_range = -2:dx:2;
extended_spatial_range = -9:dx:9;
length_scale = 1.;
oscillation_length_scale = 2.; % 1. or 2.

spatiotemporal_signal = cell(number_trials,1);
spatiotemporal_alpha_process = cell(number_trials,1);
alpha_process = cell(number_trials,1);
concatenated_data = [];
for trial = 1:number_trials
    % Alpha process
    % covariance function
    amp_1 = 0.5; %variance
    nu_1 = 0.05; % length scale
    K_amp = amp_1.^2*exp(-(T_x-T_y).^2/nu_1);
    % mean function
    m_1 = ones(length(t),1);
    r1 = mvnrnd(m_1,K_amp,N);
    p1 = sqrt(1+r1.^2) - 1;
    omega = ones(length(t),1)*10*2*pi;
    sd_2 = 0.25*2*pi; %0.4
    nu_2 = 0.04; % frequency length scale
    K_freq = sd_2.^2*exp(-(T_x-T_y).^2/nu_2);
    r2 = mvnrnd(omega,K_freq,N);
    p2 = cos(r2.*repmat(t,N,1) + 2*pi*repmat(rand(N,1),[1,length(t)]));
    alpha_process{trial} = (p1.*p2.^exponent)';
    alpha_length_scale = oscillation_length_scale;
    alpha_center = -0.5;
    alpha_spatial_profile = exp(-abs(spatial_range - alpha_center).^2/(2*alpha_length_scale^2));
    spatiotemporal_alpha_process{trial} = (alpha_process{trial}*alpha_spatial_profile)';

    %Theta process
     % covariance function
    amp_1 = 0.5; %variance
    nu_1 = 0.1; % length scale
    K_amp = amp_1.^2*exp(-(T_x-T_y).^2/nu_1);
    % mean function
    m_1 = ones(length(t),1);
    r1 = mvnrnd(m_1,K_amp,N);
    p1 = sqrt(1+r1.^2) - 1;
    omega = ones(length(t),1)*theta_frequency*2*pi;
    sd_2 = 0.25*2*pi; %0.4
    nu_2 = 0.04; % frequency length scale
    K_freq = sd_2.^2*exp(-(T_x-T_y).^2/nu_2);
    r2 = mvnrnd(omega,K_freq,N);
    p2 = cos(r2.*repmat(t,N,1) + 2*pi*repmat(rand(N,1),[1,length(t)]));
    theta_process = (p1.*p2.^exponent)';
    theta_length_scale = oscillation_length_scale;
    theta_center = 0.5;
    theta_spatial_profile = exp(-abs(spatial_range - theta_center).^2/(2*theta_length_scale^2));
    spatiotemporal_theta_process = (theta_process*theta_spatial_profile)';

    % Broadband noise
    % covariance function
    amp_4 = 0.1; %variance
    spatiotemporal_bb_process = amp_4*randn(size(spatiotemporal_theta_process));

    % Relaxation noise 1
    % covariance function
    amp_5 = 0.6; %variance
    k_5 = 0.1; % length scale
    K_bn = amp_5.^2*exp(-abs(T_x-T_y)/k_5);
    relaxation_noise = mvnrnd(zeros(length(t),1),K_bn,N)';
    rel_length_scale = length_scale;
    rel_center = 1.5;
    rel_spatial_profile = exp(-abs(spatial_range - rel_center).^2/(2*rel_length_scale^2));
    spatiotemporal_rel_process_1 = (relaxation_noise*rel_spatial_profile)';
    
    % Relaxation noise 2
    % covariance function
    amp_5 = 0.6; %variance
    k_5 = 0.1; % length scale
    K_bn = amp_5.^2*exp(-abs(T_x-T_y)/k_5);
    relaxation_noise = mvnrnd(zeros(length(t),1),K_bn,N)';
    rel_length_scale = length_scale;
    rel_center = -1.5;
    rel_spatial_profile = exp(-abs(spatial_range - rel_center).^2/(2*rel_length_scale^2));
    spatiotemporal_rel_process_2 = (relaxation_noise*rel_spatial_profile)';

    % Total signal
    spatiotemporal_signal{trial} = spatiotemporal_alpha_process{trial} + spatiotemporal_theta_process + spatiotemporal_bb_process + spatiotemporal_rel_process_1 + spatiotemporal_rel_process_2;
    concatenated_data = [concatenated_data, spatiotemporal_signal{trial}];
end



figure, imagesc(spatiotemporal_signal{1})
figure, plot(t, spatiotemporal_signal{1})


%% Signal covariance function
c_1 = 0;
for trial = 1:number_trials
    demeaned_signal = spatiotemporal_signal{trial}; %detrend(spatiotemporal_signal{trial}','constant')';
    c_1 = c_1 + demeaned_signal'*demeaned_signal/number_trials;
end

% stationarization
c_stat_1 = 0;
for j = -length(t):length(t)
    c_stat_1 = c_stat_1 + diag(mean(diag(c_1,j))*ones(length(diag(c_1,j)),1),j);
end

max_c_1 = max(max(c_stat_1));
c_norm_1 = c_stat_1/max_c_1;

figure,
plot(diag(fliplr(c_norm_1)))

%% GP regression model

% Priors parameters

% Kernel configuration
num_kernels = 4; % Number of kernels
kernels = {'exponential','damped_harmonic_oscillator_theta', 'damped_harmonic_oscillator_alpha', 'squared_exponential'}; % Type of kernels

% Prior configuration
cfg_param = [];
num_param = [ 2, 3, 3, 2];
% Each hyper-parameter can have its own (hyper-)prior distribution
prior_pdf{1} = {'logNorm', 'unif'}; %Exponential
prior_pdf{2} = {'logNorm', 'logNorm', 'beta'};
prior_pdf{3} = {'logNorm', 'logNorm', 'beta'};
prior_pdf{4} = {'logNorm','logNorm'};


% Hyper-prior parameters
prior_hyp_parameters{1} = { [ 1, 2], [0, 5]}; %Exponential
prior_hyp_parameters{2} = { [ 1, 2], [0.01, 40], [2*pi*3, 2*pi*9, 1, 1]}; %Theta
prior_hyp_parameters{3} = { [ 1, 2], [0.01, 40], [2*pi*9, 2*pi*13, 1, 1]}; %Alpha 
prior_hyp_parameters{4} = { [ 1, 2], [ 0, 1]}; 

% Variable transformations
var_transform{1} = {'log', 'logit'};
var_transform{2} = {'log','log', 'logit' };
var_transform{3} = {'log','log', 'logit' };
var_transform{4} = {'log','logit'};

transform_param{1} = {[], [0, 5]};
transform_param{2} = {[], [], [2*pi*3, 2*pi*9] };
transform_param{3} = {[], [], [2*pi*9, 2*pi*13] };
transform_param{4} = {[], [10^-30,10^-29]};

for j = 1:num_kernels
    % Parameters structure
    cfg = [];
    cfg.numparam = num_param(j); % number of (hyper-)parameters
    cfg.pdf = prior_pdf{j};
    cfg.hyparam = prior_hyp_parameters{j};
    cfg.tran = var_transform{j};
    cfg.tranparam = transform_param{j}; 
    cfg_param = setfield(cfg_param, kernels{j}, cfg);
end

% Kernel structure
cfg_k = [];
cfg_k.kernels = kernels;
cfg_k.num = num_kernels;
cfg_k.num_param = max(num_param);
% cfg_k.wavelets.centers = 0.1:0.05:0.2;
% cfg_k.wavelets.scales = (2*pi*[ 2 ]).^-1;
% cfg_k.wavelets.width = sqrt(0.05);
% cfg_k.wavelets.amp = 0.2;

% Parameters initialization
in_param = [log(1/4), log(1), log(1/4), 0, 0, log(1/4), 0, 0, log(1/4), 0  ]'  + 0.1*randn(sum(num_param),1);
                  
%% Simulated annealing 
% Finding the global mode that will be also used as a starting point for
% the monte carlo sampling
sigma = 0.1;

l = @(x) ct_least_squares4(cfg_k, cfg_param, x, c_norm_1, T_x, T_y);
g = @(x) x + randn(1)*sigma*mnrnd(1,ones(size(x))/length(x)).*trnd(2);

cfg = [];
cfg.Verbosity = 2;
cfg.InitTemp = 10;
cfg.StopTemp = 10^-9;cfg.Generator = g;
cfg.CoolSched = @(T) (.95*T);
mode = anneal(l,in_param',cfg);
lambda_n = 0.1;

%% Covariance function plot
theta = ct_transform(cfg_k, cfg_param, mode);
[K_tot K] = ct_kernel(cfg_k, theta, T_x, T_y, 0);
K_tot = max_c_1*K_tot;
K = max_c_1*K;

tau = (T_x - T_y);
k_plot = [ fliplr(K_tot(2:end,1)'), K_tot(1,1:end)];
c_plot = [ fliplr(c_norm_1(2:end,1)'), c_norm_1(1,1:end)];
t_plot = [ fliplr(tau(2:end,1)'), tau(1,1:end)];

figure, 
plot(t_plot, max_c_1*c_plot, t_plot, k_plot)
legend('Data autocovariance','Parametric fitting')

%% Spatial covariance function
l = 0.2;
order = 5;
spatial_noise = 0.1;
spatial_frequency_lim = 1/(2*dx);
spatial_frequency_step = 1/(max(extended_spatial_range) - min(extended_spatial_range));
spatial_frequency_range = -spatial_frequency_lim:spatial_frequency_step:spatial_frequency_lim;
frequency_covariance = 1./sqrt(1 + (spatial_frequency_range/l).^(2*order));
space_covariance = real(fftshift(fft(ifftshift(frequency_covariance))));

[spatial_grid_x, spatial_grid_y] = meshgrid(spatial_range, spatial_range);
difference_grid = spatial_grid_y - spatial_grid_x;
% 
space_covariance_matrix = interp1(extended_spatial_range, space_covariance, difference_grid);
total_spatial_covariance_matrix = space_covariance_matrix + spatial_noise*eye(length(spatial_range));
spatial_filter = space_covariance_matrix/total_spatial_covariance_matrix;

figure, plot(spatial_frequency_range, frequency_covariance)
figure, plot(extended_spatial_range, space_covariance)

%% Posterior mean
mean_pos = cell(number_trials,1);
mean_process = cell(number_trials,1);

[K_tot, K] = ct_kernel(cfg_k, ct_transform(cfg_k, cfg_param, mode), T_x, T_y, 0);
kernel = 3;
mean_pos = cell(number_trials,1);
for trial = 1:number_trials
    inv_K = squeeze(K(kernel,:,:))/K_tot;
    mean_pos{trial} = spatial_filter*spatiotemporal_signal{trial}*inv_K';
    [max_val, max_ind] = max(sum(abs(mean_pos{1}')));
    mean_process{trial} = mean_pos{trial}(max_ind,:);
end

figure,
imagesc(spatiotemporal_alpha_process{1}),
figure,
plot(t, mean_process{1})


%% SVD analysis
[U,D,PCA_comps] = svd(detrend(concatenated_data','constant')', 'econ');
PCA_comps = PCA_comps';

number_comps = size(PCA_comps,1);
c = zeros(number_comps,1);
for component = 1:number_comps
    c(component) = corr(PCA_comps(component,1:length(alpha_process{1}))', alpha_process{1});
end

[max_val, max_ind] = max(abs(c));
sign_corr = sign(c(max_ind));
PCA_selected_comp = sign_corr*PCA_comps(max_ind,:);

PCA_results = cell(number_trials,1);
for trial = 1:number_trials
    PCA_results{trial} = PCA_selected_comp((trial - 1)*number_time_points + 1:trial*number_time_points);
end

%%
comp = 11;
figure, 
plot(t, zscore(PCA_results{2}), t, zscore(alpha_process{1}), t, zscore(mean_process{1}))
legend('PCA','Ground truth', 'GP')


%% ICA
ICA_comps = fastica(detrend(concatenated_data','constant')');
number_comps = size(ICA_comps,1);
c = zeros(number_comps,1);
for component = 1:number_comps
    c(component) = corr(ICA_comps(component,1:length(alpha_process{1}))', alpha_process{1});
end

[max_val, max_ind] = max(abs(c));
sign_corr = sign(c(max_ind));
ICA_selected_comp = sign_corr*ICA_comps(max_ind,:);

ICA_results = cell(number_trials,1);
for trial = 1:number_trials
    ICA_results{trial} = ICA_selected_comp((trial - 1)*number_time_points + 1:trial*number_time_points);
end



%%
comp = 11;
figure, 
plot(t, zscore(ICA_results{1}), t, zscore(alpha_process{1}), t, zscore(mean_process{1}))
legend('ICA','Ground truth', 'GP')

corr(ICA_selected_comp(1:length(alpha_process{1}))', alpha_process{1})
corr(mean_process{1}', alpha_process{1})

%% EMD analysis
EMD_results = cell(number_trials,1);

for trial = 1:number_trials
    EMDcomponents = [];
    for location_index = 1:length(spatial_range)
        new_components = emd(spatiotemporal_signal{trial}(location_index,:));
        EMDcomponents = [EMDcomponents; new_components];
    end
    number_components = size(EMDcomponents,1);
    % Alpha
    correlations = zeros(number_components,1);
    for component = 1:number_components
        correlations(component) = corr(EMDcomponents(component,:)',alpha_process{trial});
    end
    ind_max_corr = find(abs(correlations)==max(abs(correlations)));
    EMD_results{trial} = EMDcomponents(ind_max_corr,:);
    disp(['EMD, trial number: ', num2str(trial)])
end

%%
trial = 29;

comp = 11;
figure, 
plot(t, zscore(EMD_results{trial}), t, zscore(alpha_process{trial}), t, zscore(mean_process{trial}))
legend('EMD','Ground truth', 'GP')

corr(EMD_results{trial}', alpha_process{trial})
corr(mean_process{trial}', alpha_process{trial})

corr(EMD_results{trial}', alpha_process{trial})
corr(mean_process{trial}', alpha_process{trial})

%% SSA analysis
num_comp = 5;
subtime_length = 0.2;
L = round(subtime_length/dt);
SSA_results = cell(number_trials,1);

for trial = 1:number_trials
    SSAcomponents = [];
    for location_index = 1:length(spatial_range)
        new_components = ssa(spatiotemporal_signal{trial}(location_index,:)', L, num_comp)';
        SSAcomponents = [SSAcomponents; new_components];
    end
    number_components = size(SSAcomponents,1);
    % Alpha
    correlations = zeros(number_components,1);
    for component = 1:number_components
        correlations(component) = corr(SSAcomponents(component,:)',alpha_process{trial});
    end
    ind_max_corr = find(abs(correlations)==max(abs(correlations)));
    SSA_results{trial} = SSAcomponents(ind_max_corr,:);
    disp(['EMD, trial number: ', num2str(trial)])
end

%%
trial = 14;

comp = 11;
%figure, 
%plot(t, zscore(SSA_results{trial}), t, zscore(alpha_process{trial}), t, zscore(mean_process{trial}))
%legend('SSA','Ground truth', 'GP')

SSA_corr = zeros(number_trials,1);
GP_corr = zeros(number_trials,1);
EMD_corr = zeros(number_trials,1);
ICA_corr = zeros(number_trials,1);
PCA_corr = zeros(number_trials,1);
for trial = 1:number_trials
    SSA_corr(trial) = abs(corr(SSA_results{trial}', alpha_process{trial}));
    EMD_corr(trial) = abs(corr(EMD_results{trial}', alpha_process{trial}));
    ICA_corr(trial) = abs(corr(ICA_results{trial}', alpha_process{trial}));
    PCA_corr(trial) = abs(corr(PCA_results{trial}', alpha_process{trial}));
    GP_corr(trial) = abs(corr(mean_process{trial}', alpha_process{trial}));
end

GP_median = median(GP_corr);
SSA_median = median(SSA_corr);
EMD_median = median(EMD_corr);
ICA_median = median(ICA_corr);
PCA_median = median(PCA_corr);

figure,
h = boxplot([GP_corr, SSA_corr, EMD_corr, ICA_corr, PCA_corr],'labels',{['GP = ', num2str(GP_median)],['SSA = ', num2str(SSA_median)],['EMD = ', num2str(EMD_median)],['ICA = ', num2str(ICA_median)], ['PCA = ', num2str(PCA_median)]});
%set(h(7,:),'Visible','off') 
ylim([0,1])
ylabel('Correlation')
xlabel('Methods')


%%


% %% SSD analysis
% sorted_SSD_components = zeros(N,length(t));
% good_trials = zeros(N,length(t));
% for trial = 1:N
%     try
%         [SSDcomponents]=SSD(signal(trial,:),1/dt,0.001,5);
%         number_components = size(SSDcomponents,1);
%         %% Theta
%         correlations = zeros(number_components,1);
%         for component = 1:number_components
%             correlations(component) = corr(SSDcomponents(component,:)',theta_process(trial,:)');
%         end
%         ind_max_corr = find(abs(correlations)==max(abs(correlations)));
%         sorted_SSD_components(trial,:) = SSDcomponents(ind_max_corr,:);
%         good_trials(trial) = 1;
% 
%         disp(['SSD, trial number: ', num2str(trial)])
%     catch
%         disp('Failure')
%     end
% end
% 
% trial_indices = 1:N;
% trial_indices = trial_indices(logical(good_trials));
% 
% %% EMD analysis
% 
% sorted_EMD_components = zeros(N,length(t));
% 
% for trial = 1:N
%     EMDcomponents = emd(signal(trial,:));
%     number_components = size(EMDcomponents,1);
%     %% Theta
%     correlations = zeros(number_components,1);
%     for component = 1:number_components
%         correlations(component) = corr(EMDcomponents(component,:)',theta_process(trial,:)');
%     end
%     ind_max_corr = find(abs(correlations)==max(abs(correlations)));
%     sorted_EMD_components(trial,:) = EMDcomponents(ind_max_corr,:);
%     
%     disp(['EMD, trial number: ', num2str(trial)])
% end
% 
% 
% %% Wavelets
% %close all
% % wavelet_error = zeros(15,1);
% % vectorized_signal = reshape(signal',N*length(t),1);
% % kernel_time = -T/2:dt:T/2;
% % theta_angular_frequency = theta_frequency*2*pi;
% %     
% % for number_cycles = 1:15
% %     % theta filter
% %     theta_width = number_cycles/theta_angular_frequency;
% %     theta_kernel = exp(-kernel_time.^2/(2*theta_width^2)).*cos(theta_angular_frequency*kernel_time);
% %     theta_vectorized_signal = conv(vectorized_signal, theta_kernel, 'same');
% %     wavelet_component = reshape(theta_vectorized_signal,length(t),N)';
% %     wavelet_error(number_cycles) = 0;
% %     for trial = 1:N
% %         wavelet_error(number_cycles) = wavelet_error(number_cycles) + sum((zscore(taper.*theta_process(trial,:)) - zscore(wavelet_component(trial,:))).^2);
% %     end 
% % end
% % 
% % number_cycles = find(wavelet_error==min(wavelet_error));
% % 
% % % theta filter
% % theta_width = number_cycles/theta_angular_frequency;
% % theta_kernel = exp(-kernel_time.^2/(2*theta_width^2)).*cos(theta_angular_frequency*kernel_time);
% % theta_vectorized_signal = conv(vectorized_signal, theta_kernel, 'same');
% % wavelet_component = reshape(theta_vectorized_signal,length(t),N)';
% 
% M = length(trial_indices);
% trial_to_plot = 3;
% theta_GP_error = zeros(M,1);
% %theta_wavelet_error = zeros(M,1);
% theta_SSD_error = zeros(M,1);
% theta_EMD_error = zeros(M,1);
% for trial = 1:M
%     index = trial_indices(trial);
%     theta_GP_error(trial) = mean(abs(zscore(taper.*theta_process(index,:)) - zscore(mean_pos{2}(index,:))).^2)/M;
%     %theta_wavelet_error(trial) = mean(abs(zscore(taper.*theta_process(index,:)) - zscore(wavelet_component(index,:))).^2)/M;
%     theta_SSD_error(trial) = mean(abs(zscore(taper.*theta_process(index,:)) - zscore(sorted_SSD_components(index,:))).^2)/M;
%     theta_EMD_error(trial) = mean(abs(zscore(taper.*theta_process(index,:)) - zscore(sorted_EMD_components(index,:))).^2)/M;
% end
% 
% figure,
% h = boxplot([theta_GP_error, theta_SSD_error, theta_EMD_error], 'Labels',{'GP','SSD','EMD'});
% title('High noise, non-linear, single')
% ylabel('Mean squared error')
% xlabel(['GP:',num2str(mean(theta_GP_error)), 'SSD:',num2str(mean(theta_SSD_error)),'EMD:', num2str(mean(theta_EMD_error))])
% ylim([0,10]*10^-3)
% %h = boxplot([theta_GP_error, theta_wavelet_error, theta_EMD_error]);
% %h = boxplot([theta_GP_error, theta_wavelet_error])
% set(h(7,:),'Visible','off')
% 
% %figure,
% %scatter(theta_GP_error, theta_wavelet_error)
% %hold on
% %scatter(mean(theta_GP_error),mean(theta_wavelet_error), 'fill')
% %hold on
% %title(['mean GP error: ', num2str(mean(theta_GP_error)), 'mean wavelets error: ', num2str(mean(theta_wavelet_error))])
% %plot(0:0.01:1,0:0.01:1)





















