% Neural signal detection simulations
close all

kov = 1;
gauss_a = 0;

%n = 0:0.1:0.6;

%sig = 0.1:0.3:0.7;
sig = 0.15:0.03:0.6;
filt_GP = zeros(size(sig));
filt_NP_dd = zeros(size(sig));
filt_NP = zeros(size(sig));

for k1=1:length(sig)
%% Constants
dt = 1/150;
T = 0.7;
t = 0:dt:T;
t_test_ind = find(t>0.1&t<0.5);
N = 150000;
mu_amp = [0, sig(k1)]; %signal amplitude
[T_x T_y] = meshgrid(t,t); % For constructing the kernel
signal = cell(2,1);

for j = 1:2
    %% Non-linear alpha amplitude process
    % covariance function
    amp_1 = 0.00001; %variance
    nu_1 = 0.2; % length scale
    K_amp = amp_1.^2*exp(-(T_x-T_y).^2/nu_1);
    % mean function
    m_1 = 1;
    
    %nu_m = 0.5; %signal width
    %c_m = 0.7; %signal center
    mean_amp = m_1*ones(length(t),1) + mu_amp(j); %*normpdf(t,c_m,nu_m);

    r1 = mvnrnd(mean_amp,K_amp,N);
    p1 = sqrt(1+r1.^2) - 1;
    %figure, plot(t, p1)

    %% Non-stationary oscillatory process
    omega = ones(length(t),1)*10*2*pi;
    sd_2 = 0.4*2*pi;
    nu_2 = 0.2; % frequency length scale
    K_freq = sd_2.^2*exp(-(T_x-T_y).^2/nu_2);

    r2 = mvnrnd(omega,K_freq,N);
    p2 = cos(r2.*repmat(t,N,1) + 2*pi*rand(1));
    %figure, plot(t, p2)

    %% Alpha oscillations
    
    if gauss_a == 1
        amp_1 = 0.2; %variance
        nu_1 = 0.05; % length scale
        K_alpha = (amp_1+mu_amp(j)).^2*exp(-(T_x-T_y).^2/nu_1).*cos(2*pi*10*abs(T_x-T_y));
        % mean function
        m_1 = 1;
        %nu_m = 0.5; %signal width
        %c_m = 0.7; %signal center
        mean_amp = m_1*ones(length(t),1) + mu_amp(j); %*normpdf(t,c_m,nu_m);
        p3 = mvnrnd(zeros(size(t))',K_alpha,N);
    else
       p3 = p1.*p2;
    end

    %% Broadband noise
    % covariance function
    amp_4 = 0.55; %variance
    nu_4 = dt/8; % length scale
    K_bn = amp_4.^2*exp(-(T_x-T_y).^2/nu_4);

    p4 = mvnrnd(zeros(length(t),1),K_bn,N);
    %figure, plot(t, p4)

    %% Pink noise
    % covariance function
    amp_5 = 0.55; %variance
    k_5 = 0.2; % length scale
    K_bn = amp_5.^2*exp(-abs(T_x-T_y)/k_5);

    p5 = mvnrnd(zeros(length(t),1),K_bn,N);
    %figure, plot(t, p5)

    %% Total noisy signal

    signal{j} = p3 + p4 + p5;
    %figure, plot(t, signal(1,:))

    %% Spectra
    s3 = pwelch(p3');
    s4 = pwelch(p4');
    s5 = pwelch(p5');

    figure,
    plot(1:length(s3), s3, 1:length(s4), s4, 1:length(s5), s5)
end

%% Spectral analysis (power difference)
df = 0.2:0.2:6;
raw_z = zeros(length(df),1);
for j = 1:length(df)
    omega_c = 2*pi*10;
    basis = exp(1i*omega_c*t)'/numel(t);
    dps_seq = dpss(length(t_test_ind),df(j)*2);
    basis_tp = dps_seq.*repmat(basis(t_test_ind),1,size(dps_seq,2));

    F_1 = signal{1}(:,t_test_ind)*basis_tp;
    F_2 = signal{2}(:,t_test_ind)*basis_tp;

    P_avg_1 = sqrt(mean(abs(F_1).^2,2));
    P_avg_2 = sqrt(mean(abs(F_2).^2,2));

    raw_z(j) = 2*(mean(P_avg_1) - mean(P_avg_2))./(std(P_avg_1) + std(P_avg_2));

    %figure,
    %boxplot([P_avg_1,P_avg_2])
    j
end

%% Signal covariance function
c_1 = cov(signal{1});

% stationarization
c_stat_1 = 0;
for j = -length(t):length(t)
    c_stat_1 = c_stat_1 + diag(mean(diag(c_1,j))*ones(length(diag(c_1,j)),1),j);
end


max_c_1 = max(max(c_stat_1));
c_norm_1 = c_stat_1/max_c_1;


c_2 = cov(signal{2});

% stationarization
c_stat_2 = 0;
for j = -length(t):length(t)
    c_stat_2 = c_stat_2 + diag(mean(diag(c_2,j))*ones(length(diag(c_2,j)),1),j);
end


max_c_2 = max(max(c_stat_2));
c_norm_2 = c_stat_2/max_c_2;

figure,
plot(diag(fliplr(c_norm_1)))

if kov == 1

%% GP regression model

% Priors parameters

% Kernel configuration
num_kernels = 3; % Number of kernels
kernels = {'exponential','damped_harmonic_oscillator_alpha', 'squared_exponential'}; % Type of kernels

% Prior configuration
cfg_param = [];
num_param = [ 2, 3, 2];
% Each hyper-parameter can have its own (hyper-)prior distribution
prior_pdf{1} = {'logNorm', 'unif'}; %Exponential
prior_pdf{2} = {'logNorm', 'logNorm', 'beta'};
prior_pdf{3} = {'logNorm','unif'};


% Hyper-prior parameters
prior_hyp_parameters{1} = { [ 1, 2], [0, 5]}; %Exponential
prior_hyp_parameters{2} = { [ 1, 2], [0.01, 40], [2*pi*8, 2*pi*13, 1, 1]}; %Alpha 
prior_hyp_parameters{3} = { [ 1, 2], [ 0, 0.03]}; 

% Variable transformations
var_transform{1} = {'log', 'logit'};
var_transform{2} = {'log','log', 'logit' };
var_transform{3} = {'log','logit'};

transform_param{1} = {[], [0, 5]};
transform_param{2} = {[], [], [2*pi*8, 2*pi*13] };
transform_param{3} = {[], [0, 0.03]};

for j = 1:num_kernels
    % Parameters structure
    cfg = [];
    cfg.numparam = num_param(j); % number of (hyper-)parameters
    cfg.pdf = prior_pdf{j};
    cfg.hyparam = prior_hyp_parameters{j};
    cfg.tran = var_transform{j};
    cfg.tranparam = transform_param{j}; 
    cfg_param = setfield(cfg_param, kernels{j}, cfg);
end

% Kernel structure
cfg_k = [];
cfg_k.kernels = kernels;
cfg_k.num = num_kernels;
cfg_k.num_param = max(num_param);
% cfg_k.wavelets.centers = 0.1:0.05:0.2;
% cfg_k.wavelets.scales = (2*pi*[ 2 ]).^-1;
% cfg_k.wavelets.width = sqrt(0.05);
% cfg_k.wavelets.amp = 0.2;

% Parameters initialization
in_param = [log(1/4), log(1), log(1/4), 0, 0, log(1/4), 0  ]'  + 0.1*randn(sum(num_param),1);
                  
%% Simulated annealing 
% Finding the global mode that will be also used as a starting point for
% the monte carlo sampling
sigma = 0.1;

l = @(x) ct_least_squares4(cfg_k, cfg_param, x, 0.5*(c_norm_1 + c_norm_2), T_x, T_y);
g = @(x) x + randn(1)*sigma*mnrnd(1,ones(size(x))/length(x)).*trnd(2);

if k1 == 1
    cfg = [];
    cfg.Verbosity = 2;
    cfg.InitTemp = 100;
    cfg.StopTemp = 10^-6;cfg.Generator = g;
    mode = anneal(l,in_param',cfg);
end

lambda_n = 0.1;


%% Covariance function plot

theta = ct_transform(cfg_k, cfg_param, mode);
[K_tot K] = ct_kernel(cfg_k, theta, T_x, T_y, 0);
K_tot = max_c_1*K_tot;
K = max_c_1*K;

tau = (T_x - T_y);
k_plot = [ fliplr(K_tot(2:end,1)'), K_tot(1,1:end)];
c_plot = [ fliplr(c_norm_1(2:end,1)'), c_norm_1(1,1:end)];
t_plot = [ fliplr(tau(2:end,1)'), tau(1,1:end)];

figure, 
plot(t_plot, max_c_1*c_plot, t_plot, k_plot)
legend('Data autocovariance','Parametric fitting')

end

%% Posterior mean 

mean_pos = cell(3,2);


[K_tot, K] = ct_kernel(cfg_k, ct_transform(cfg_k, cfg_param, mode), T_x, T_y, 0);
for k = 2:2
    inv_K = squeeze(K(k,:,:))/K_tot;
    for trial = 1:N
        mean_pos{k,1}(:,trial) = signal{1}(trial,:)*inv_K';
        mean_pos{k,2}(:,trial) = signal{2}(trial,:)*inv_K';
        clc
        disp(['Posterior computation, ',' Kernel: ', num2str(k), ' Trial: ', num2str(trial)])
    end
end

%% plot reconstructions

%figure,
%plot(t(t_test_ind), mean_pos{2,2}(t_test_ind,2), t(t_test_ind), p3(2,t_test_ind))

%figure,
%plot(t(t_test_ind), mean_pos{2,2}(t_test_ind,2), t(t_test_ind), signal{2}(2,t_test_ind))

%% Alpha amplitude effect

omega_c = 2*pi*10;
basis = exp(1i*omega_c*t)';

F_1 = std(mean_pos{2,1}(t_test_ind,:));
F_2 = std(mean_pos{2,2}(t_test_ind,:));

filt_z = 2*(mean(abs(F_1)) - mean(abs(F_2)))./(std(abs(F_1)) + std(abs(F_2)));

%figure,
%boxplot([F_1',F_2'])


%figure, 
%plot(raw_z), hold on, plot(ones(length(raw_z),1)*filt_z)

filt_GP(k1) = filt_z;
filt_NP_dd(k1) = min(raw_z);
filt_NP(k1) = raw_z(3);

end
close all

%%
figure, 
plot(sig, abs(filt_GP), sig, abs(filt_NP_dd), sig, abs(filt_NP))
title('Effect size comparison')
xlabel('Amplitude difference')
ylabel('Estimated effect size')
legend('Gaussian Process','Multitaper (Optimal)','Multitaper (0.6Hz)')

figure, 
plot(sig, abs(filt_GP)./sig, sig, abs(filt_NP_dd)./sig, sig, abs(filt_NP)./sig )
title('Normalized effect size comparison')
xlabel('Amplitude difference')
ylabel('Estimated normalized effect size')
legend('Gaussian Process','Multitaper (Optimal)','Multitaper (0.6Hz)')

figure, 
plot(sig, abs(filt_GP)./abs(filt_NP_dd))
title('Effect size ratio')
xlabel('Amplitude difference')
ylabel('(GP effect size)/(Optimal Multitaper effect size)')

%% Saving
sim_result_ch = [];
sim_result_ch.GP = filt_GP;
sim_result_ch.multitaper = raw_z;
sim_result_ch.sgnal = sig;

path_to_save = '/home/lucamb/GP final figures/Simulations/Result files/';
save([path_to_save,'ch_results'],'sim_result_ch')



















